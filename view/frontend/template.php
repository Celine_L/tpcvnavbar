<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="stylesheet" href="public/css/bootstrap.css" />
        <link rel="stylesheet" href="public/css/global.css"/>
        <link rel="icon"       href="public/img/faviconpasteque.ico" />
        <title><?= $title ?></title>
    </head>

     <body class="container-fluid">
            <header class="row">
                <div class="col-12">
                    <h1>TemplateE</h1>
					<p>Nav bar non responsive Nav bar non responsive Nav bar non responsive</p>
                        <nav class="navbar navbar-expand navbar-light">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <?php $NumPartie = 1; ?>
                                    <a href="index.php?action=partie&amp;idPartie=<?= $NumPartie ?>" class="nav-link" target="_blank">Ma formation</a>
                                </li>
                                <li class="nav-item">

                                <li class="nav-item">
                                    <?php $NumPartie = 2; ?>
                                    <a href="index.php?action=partie&amp;idPartie=<?= $NumPartie ?>" class="nav-link" target="_blank">Mes compétences</a>
                                </li>
                                <li class="nav-item">

                                <li class="nav-item">
                                    <?php $NumPartie = 3; ?>
                                    <a href="index.php?action=partie&amp;idPartie=<?= $NumPartie ?>" class="nav-link" target="_blank">Mes expériences professionnelles</a>
                                </li>

                                <li class="nav-item">
                                    <?php $NumPartie = 4; ?>
                                    <a href="index.php?action=partie&amp;idPartie=<?= $NumPartie ?>" class="nav-link" target="_blank">Mes coordonnées</a>
                                </li>

                                <li class="nav-item">
                                    <?php $NumPartie = 5; ?>
                                    <a href="index.php?action=partie&amp;idPartie=<?= $NumPartie ?>" class="nav-link" target="_blank"><?php echo 'Affichage id passé dans l URL '.$NumPartie;?></a>
                                </li>
                            </ul>
                         </nav>
                </div>
            </header>


            <section class="row">
                 <div class="col-lg-12">

                    <h1>Nom de la view appelée: <?= $nomdelaview ?></h1>
                    <p>
                        <!-- Date en PHP <?php echo date('d/m/Y h:i:s'); ?>.<br/> -->
                        Test template<br/>
                    </p>
                    <?= $content ?>
                </div>
            </section>

            <footer class="row">
                <div class="col-lg-12">
                    ------------------------
                    <div class="italic">
                        Un programme informatique fait ce que vous lui avez dit de faire, pas ce que vous voulez qu'il fasse
                    </div>
                    Loi de Murphy
                </div>
            </footer>
        </body>
</html>






