                  <h1>Compétences</h1>
                  <div class="textjustifymargin">
                          <h2>Langages de programmation</h2>
                            <ul>
                              <li>C</li>
                              <li>C++ (Library OpenGL)</li>
                              <li>Java SE (Library Swing)</li>
                              <li>Kotlin</li>
                              <li>Objective C - Swift</li>
                              <li>Sql</li>
                              <li>Visual basic</li>
                            </ul>

                            <h2>Langages web</h2>
                            <ul>
                              <li>Angular</li>
                              <li>CSS (Bootstrap)</li>
                              <li>HTML</li>
                              <li>JavaScript</li>
                              <li>PHP</li>
                            </ul>

                          <h2>SGBD</h2>
                            <ul>
                              <li>Access</li>
                              <li>MySQL</li>
                            </ul>

                           <h2>Logiciels Serveurs</h2>
                            <ul>
                              <li>Apache</li>
                              <li>Node.js</li>
                            </ul>

                          <h2>Déploiement</h2>
                            <ul>
                              <li>Amazon Web Services : Services S3, Certificate Manager, CloudFront, Route 53, EC2, RDS</li>
                            </ul>

                          <h2>Protocoles de com</h2>
                            <ul>
                              <li>I2C / RS232</li>
                              <li>Ethernet</li>
                              <li>Bluetooth</li>
                              <li>Wi-Fi 802.11</li>
                            </ul>


                          <h2>Analyse statistique</h2>
                            <ul>
                              <li>Minitab</li>
                              <li>SAS</li>
                            </ul>

                          <h2>Conception</h2>
                            <ul>
                              <li>Architecture et programmation microcontrôleur</li>
                              <li>Architecture Modèle - Vue - Contrôleur  (MVC)</li>
                              <li>Brain Computer Interface (BCI)</li>
                              <li>Git</li>
                              <li>Modèle Entité Association</li>
                              <li>Unified Modeling Language (UML)</li>
                              <li>Systèmes embarqués, temps réel</li>
                            </ul>
                  </div>