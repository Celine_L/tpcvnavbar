                      <div>
                        <h1>Expériences</h1>
                        <div class="textjustifymargin">
                            <p>
                              Développement d’un site web<br />
                              Déploiement sur aws <br />
                              Technologies utilisées : PHP - MySQL - Bootstrap - AWS - Protocoles SSL et HTTPS - Git<br />
                              Espace de connexion partagé avec l'app Recall<br />
                              Recall the App - Paris<br />
                              2019
                            </p>

                            <p>
                              Acquisition et affichage de données<br />
                              Technologies utilisées : PHP - MySQL - Langage C<br />
                              Programmation du microcontrôleur mbed NXP LPC1768 relié au capteur ultrason HC-SR04 : calcul des distances entre le capteur ultrason HC-SR04 et les objets rencontrés<br />
                              Création d'une base de données MySQL pour le stockage des distances calculées<br />
                              Développement d'un site web pour l'affichage de ces données<br />
                              Université Paris 8<br />
                              2019
                            </p>

                            <p>
                              Développement d'une application Androïd<br />
                              Technologies utilisées : Langage Kotlin - IDE Androïd Studio - SQLite<br />
                              Chargement aléatoire d'une image pour chaque produit inséré, <br />
                              Demande de confirmation de l'ajout du produit, <br />
                              Possibilité de différer l’ajout d’un produit, <br />
                              Utilisation de la fonction de recherche. <br />
                              Université Paris 8 <br />
                              2019
                            </p>
                        </div>
                      </div>




