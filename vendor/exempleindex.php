<?php
	// Initialisation
	include 'app/global/config/init.php';
	// Début de la tamporisation de sortie
	ob_start();
	// Si un module est specifiéon regarde s'il existe
	if (!empty($_GET['module'])) {
		// $module = dirname(__FILE__).'\app\modules\\'.$_GET['module'].'/';
		$module = dirname(__FILE__).'/app/modules/'.$_GET['module'].'/';
		// $module = '/app/modules/'.$_GET['module'].'/';
		// Si l'action est specifiée, on l'utilise, sinon, on tente une action par début
		$action = (!empty($_GET['action'])) ? $_GET['action'].'.php' : 'index.php';
		// Si l'action existe, on l'exécute
		if (is_file($module.$action)) {
			include $module.$action;
		// Sinon, on affiche la page d'accueil !
		} else {
			include 'app/modules/homepage/homepage.php';
		}
	// Module non specific ou invalid ? On affiche la page d'accueil !
	}else {
		include 'app/modules/homepage/homepage.php';
	}
	// Fin de la tamporisation de sortie
	$contenu = ob_get_clean();
	// Début du code HTML
	include 'app/global/header.php';
	// echo realpath('index.php');

	// Corps du code HTML
	echo $contenu;
	// Fin du code HTML
	include 'app/global/footer.php';
?>

